package com.emedinaa.kotlinapp.tabs

import android.graphics.Color
import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.emedinaa.kotlinapp.R
import kotlinx.android.synthetic.main.activity_tapp_.*

class Tapp_Activity : AppCompatActivity(),View.OnClickListener {



    private val FRAGMENT_INICIO = 0
    private val FRAGMENT_TAREAS = 1
    private val FRAGMENT_EXAMENES = 2


    private val indicatorViews= mutableListOf<View>()
    private val titleViews= mutableListOf<TextView>()
    private var currentIndicator = -1


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tapp_)

        indicatorViews.add(ivInicio)
        indicatorViews.add(iviTarea)
        indicatorViews.add(iviExamen)

        titleViews.add(tvInicio)
        titleViews.add(tvTarea)
        titleViews.add(tvExamen)


        titleViews.forEach {
            it.setOnClickListener(this)
        }

    selectFirst()

    }

    private fun selectFirst() {
        val bundle = Bundle()
        val fragmentId = FRAGMENT_INICIO
        updateUI(fragmentId)
        changeFragment(bundle, fragmentId)
    }


    private fun updateUI(fragmentId: Int) {
        if (currentIndicator >= 0) {
            indicatorViews[currentIndicator].setBackgroundColor(Color.TRANSPARENT)
            titleViews[currentIndicator].inputType = Typeface.NORMAL
        }
        indicatorViews[fragmentId].setBackgroundColor(Color.parseColor("#ffeb3b"))
        titleViews[fragmentId].inputType = Typeface.BOLD
        currentIndicator = fragmentId
    }

    private fun changeFragment(bundle: Bundle, fragmentId: Int) {

        val fragment = factoryFragment(bundle, fragmentId)
        //fragment.setArguments(bundle);

        fragment?.let {
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.frameContainer, it)
            transaction.addToBackStack(null)

            // Commit the transaction
            transaction.commit()
        }
    }
    private fun factoryFragment(bundle: Bundle, fragmentId: Int): Fragment? {
        when (fragmentId) {
            FRAGMENT_INICIO -> return Inicios()
            FRAGMENT_TAREAS -> return Tareas()
            FRAGMENT_EXAMENES -> return Examenes()
        }
        return null
    }

    override fun onClick(p0: View?) {
        val bundle = Bundle()

        var fragmentId = when(p0?.id){
            R.id.tvInicio->  FRAGMENT_INICIO
            R.id.tvTarea->  FRAGMENT_TAREAS
            R.id.tvExamen->  FRAGMENT_EXAMENES

            else -> FRAGMENT_INICIO
        }

        updateUI(fragmentId);
        changeFragment(bundle,fragmentId)
    }

}