package com.emedinaa.kotlinapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.emedinaa.kotlinapp.R
import com.emedinaa.kotlinapp.model.VerduraEntity

class VerduraAdapter (private  val context:Context,private val verduras:List<VerduraEntity>):BaseAdapter(){
    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
        val inflater = LayoutInflater.from(context)
        val container = inflater.inflate(R.layout.card_verdura, null)
        val ivImagen=container.findViewById<ImageView>(R.id.ivImagen)
        val tvNombre=container.findViewById<TextView>(R.id.tvtNombre)
        val tvPrecio=container.findViewById<TextView>(R.id.tvPrecio)

        val verduraEntity = verduras[p0]

        ivImagen.setImageResource(verduraEntity.foto)
        tvNombre.text=verduraEntity.nombre
        tvPrecio.text=verduraEntity.precio

        return container
    }

    override fun getItem(position: Int)=verduras[position]

    override fun getItemId(p0: Int): Long =0

    override fun getCount(): Int {
       return verduras.count()
    }


}