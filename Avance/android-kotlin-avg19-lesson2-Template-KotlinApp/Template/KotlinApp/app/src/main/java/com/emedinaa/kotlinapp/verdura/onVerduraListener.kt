package com.emedinaa.kotlinapp.verdura

import com.emedinaa.kotlinapp.model.VerduraEntity

interface onVerduraListener {

    fun selectedItem(verdura: VerduraEntity)
    fun renderFirst(verdura: VerduraEntity?)
}